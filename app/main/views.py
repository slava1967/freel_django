from django.shortcuts import render


def index(request):
    context = {
        'title': 'Django Test project',
        'banner_headline_alt': 'Домашнее задание',
        'banner_headline_span': 'Разворачивание проекта и настройки CI/CD',
    }
    return render(request, 'main/index.html', context)


def about(request):
    context = {
        'title': 'О нас'
    }
    return render(request, 'main/about.html', context)
