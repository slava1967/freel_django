#!/bin/sh

ssh -o StrictHostKeyChecking=no -vv root@$IP_ADDRESS -p 49213 << 'ENDSSH'
  cd /app
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $IMAGE:web
  docker pull $IMAGE:nginx
  docker-compose -f docker-compose.yml up -d
ENDSSH